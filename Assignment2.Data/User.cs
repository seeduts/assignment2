namespace Assignment2.Data
{
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class User
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string UserType { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Username cannot be null")]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password cannot be null")]
        [StringLength(20)]
        public string Password { get; set; }

        [StringLength(50)]
        public string District { get; set; }

        public double? MaximumHours { get; set; }

        public double? MaximumCost { get; set; }

        [StringLength(50)]
        public string EmailAddress { get; set; }

    }
}
