namespace Assignment2.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Intervention
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string InterventionType { get; set; }

        public int ClientId { get; set; }

        public double Labour { get; set; }

        public double Cost { get; set; }

        public int ProposerId { get; set; }

        [Column(TypeName = "date")]
        public DateTime PerformedDate { get; set; }

        [Required]
        [StringLength(10)]
        public string State { get; set; }

        public int? ApproverId { get; set; }

        [Column(TypeName = "text")]
        public string Notes { get; set; }

        public int? RemainingLife { get; set; }

        [Column(TypeName = "date")]
        public DateTime? RecentVist { get; set; }


    }
}
