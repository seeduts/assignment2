﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Data
{
    public class EngineerTotalCosts
    {
        public string Engineer { get; set; }
        public double TotalHours { get; set; }
        public double TotalCosts { get; set; }
    }

    public class EngineerAverageCosts
    {
        public string Engineer { get; set; }
        public double AverageHours { get; set; }
        public double AverageCosts { get; set; }
    }

    public class EngineerTotalCostsModel
    {
        public List<EngineerTotalCosts> totalCostsModel { get; set; }
    }

    public class EngineerAverageCostsModel
    {
        public List<EngineerAverageCosts> averageCostsModel { get; set; }
    }
}
