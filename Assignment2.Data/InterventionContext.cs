namespace Assignment2.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.Infrastructure;
    using System.Runtime.Remoting.Messaging;
    using System.Data.Entity.SqlServer;

    public partial class InterventionContext :DbContext
    {
        public InterventionContext()
            : base("name=InterventionContext")
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<Intervention> Interventions { get; set; }
        public virtual DbSet<InterventionType> InterventionTypes { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.LocationInfo)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.District)
                .IsUnicode(false);

            modelBuilder.Entity<District>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Intervention>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<Intervention>()
                .Property(e => e.InterventionType)
                .IsUnicode(false);

            modelBuilder.Entity<Intervention>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<InterventionType>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserType)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.District)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
               .Property(e => e.EmailAddress)
               .IsUnicode(false);
        }
    }

  
}
