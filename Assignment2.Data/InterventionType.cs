namespace Assignment2.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class InterventionType
    {
        [Key]
        [StringLength(50)]
        public string Name { get; set; }

        public double EstimatedLabour { get; set; }

        public double EstimatedMaterials { get; set; }

    }
}
