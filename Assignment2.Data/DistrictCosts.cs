﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace Assignment2.Data
{
    public class TotalDistrictCosts
    {
        public string District { get; set; }
        public double TotalHours { get; set; }
        public double TotalCosts { get; set; }
    }

    public class TotalDistrictCostModel
    {
        public List<TotalDistrictCosts> districtCosts { get; set; }

        public double LabourGrandTotal { get; set; }
        public double CostGrandTotal { get; set; }
    }  

    public class MonthlyDistrictCost
    {
        public string month { get; set; }
        public double monthlyLabourTotal { get; set; }
        public double monthlyCostTotal { get; set; }
        
    }

    public class MonthlyDistrictCostModel
    {
        public string District { get; set; }
        public List<string> Districts { get; set; }
        public IEnumerable<SelectListItem> Items { get { return new SelectList(Districts); } }
        public List<MonthlyDistrictCost> model { get; set; }
    }
}
