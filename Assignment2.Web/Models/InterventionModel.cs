﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Assignment2.Web.Models
{
    public class InterventionModel
    {
        public int Id { get; set; }
        public string InterventionType { get; set; }
        public string Client { get; set; }
        public double Labour { get; set; }
        public double Cost { get; set; }
        public string Proposer { get; set; }
        [Required(ErrorMessage = "Please select a date.")]
        public DateTime PerformedDate { get; set; }
        public string State { get; set; }
        public string Approver { get; set; }
        public string Notes { get; set; }
        public int? RemainingLife { get; set; }
        public DateTime? RecentVist { get; set; }
    }

    public class InterventionModels
    {
        public List<InterventionModel> interventionModels { get; set; }
    }

    public class CreateInterventionModel
    {
        public List<string> interventionTypes { get; set; }
        public string interventionType { get; set; }
        public IEnumerable<SelectListItem> typeItems
        {
            get { return new SelectList(interventionTypes); }
        }
        public string state { get; set; }
        public List<string> states { get; set; }

        public IEnumerable<SelectListItem> stateItems
        {
            get { return new SelectList(states); }
        }
        [Required]
        [RegularExpression("^\\d+$", ErrorMessage = "Positive Digits only.")]
        public double Cost { get; set; }
        [Required]
        [RegularExpression("^\\d+$", ErrorMessage = "Positive Digits only.")]
        public double Labour { get; set; }
        public string Client { get; set; }
        [Required(ErrorMessage = "Please select a date.")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/2016}", ApplyFormatInEditMode = true)]
        public DateTime PerformedDate { get; set; }
    }

    public class QualityManagementModel
    {
        public string Notes { get; set; }
        [RegularExpression("^\\d+$", ErrorMessage = "Positive Digits only.")]
        public int? RemainingLife { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/2016}", ApplyFormatInEditMode = true)]
        public DateTime? RecentVist { get; set; }
    }

    public class ChangeStateModel
    {
        public string state { get; set; }
        public List<string> states { get; set; }

        public IEnumerable<SelectListItem> stateItems
        {
            get { return new SelectList(states); }
        }
            
    }
}