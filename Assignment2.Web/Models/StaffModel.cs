﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment2.Web.Models
{
    public class StaffModel
    {
        public string Name { get; set; }
        public string Role { get; set; }
        public string District { get; set; }
        
    }

    public class StaffModels
    {
    
        public List<StaffModel> staffModels { get; set; }
    }

    public class ChangeDistrictModel
    {
        public string Name { get; set; }
        public string District { get; set; }
        public List<string> Districts { get; set; }
        public IEnumerable<SelectListItem> Items { get { return new SelectList(Districts); } }
    }
}