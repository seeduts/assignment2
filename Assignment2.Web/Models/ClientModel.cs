﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment2.Web.Models
{
    public class ClientModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter client name.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter the client's Location Information.")]
        public string LocationInfo { get; set; }

        [Required]
        public string District { get; set; }
    }

    public class ClientModels
    {
        public List<ClientModel> clientmodels { get; set; }
    }

    public class ClientDetailModel
    {
        public ClientModel client { get; set; }
        public InterventionModels interventions { get; set; }
    }
}