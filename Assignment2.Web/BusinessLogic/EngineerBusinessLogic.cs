﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment2.Service;
using Assignment2.Web.Models;
using System.Web.Mvc;

namespace Assignment2.Web.Business_Logic
{
    public class EngineerBusinessLogic
    {
        public ClientService clientService;
        public InterventionSerivce interventionService;
        public UserService userService;
        public InterventionTypeService typeService;

        public EngineerBusinessLogic()
        {
            clientService = new ClientService();
            interventionService = new InterventionSerivce();
            userService = new UserService();
            typeService = new InterventionTypeService();
        }
        public List<ClientModel> GetClientsInEngineerDistrict(string district)
        {
            var clients = clientService.GetClientsInSameDistrict(district).ToArray();           
            List<ClientModel> models = new List<ClientModel>();
            for (int i = 0; i < clients.Length; i++)
            {
                ClientModel model = new ClientModel();
                model.Id = clients[i].Id;
                model.Name = clients[i].Name;
                model.District = clients[i].District;
                model.LocationInfo = clients[i].LocationInfo;
                models.Add(model);
            }
            return models;
        }

        public List<InterventionModel> GetInterventionsCreatedByUser(int userId)
        {
            var interventions = interventionService.getInterventionByUserId(userId).ToArray();           
            List<InterventionModel> models = new List<InterventionModel>();
            for (int i = 0; i < interventions.Length; i++)
            {
                InterventionModel model = new InterventionModel();
                model.Id = interventions[i].Id;
                model.InterventionType = interventions[i].InterventionType;
                model.Client = clientService.GetClientById(interventions[i].ClientId).Name;
                model.Labour = interventions[i].Labour;
                model.Cost = interventions[i].Cost;
                model.Proposer = userService.GetUserByUserId(interventions[i].ProposerId).Name;
                model.PerformedDate = interventions[i].PerformedDate;
                model.State = interventions[i].State;
                if(interventions[i].ApproverId.HasValue)
                model.Approver= userService.GetUserByUserId(interventions[i].ApproverId.GetValueOrDefault()).Name;
                model.Notes = interventions[i].Notes;
                    model.RemainingLife = interventions[i].RemainingLife;
                model.RecentVist = interventions[i].RecentVist;
                models.Add(model);
            }
            return models;
        }

        public ClientDetailModel GetClientDetail(string clientName)
        {
            ClientDetailModel clientDeatil = new ClientDetailModel();
            var client = clientService.GetClientByName(clientName);
            var interventions = interventionService.getInterventionsByClientId(client.Id).ToArray();
            clientDeatil.client = new ClientModel();
            clientDeatil.interventions = new InterventionModels();
            clientDeatil.interventions.interventionModels = new List<InterventionModel>();
            clientDeatil.client.Id = client.Id;
            clientDeatil.client.Name = client.Name;
            clientDeatil.client.LocationInfo = client.LocationInfo;
            clientDeatil.client.District = client.District;
            for (int i = 0; i < interventions.Length; i++)
            {
                InterventionModel model = new InterventionModel();
                model.Id = interventions[i].Id;
                model.InterventionType = interventions[i].InterventionType;
                model.Client = clientService.GetClientById(interventions[i].ClientId).Name;
                model.Labour = interventions[i].Labour;
                model.Cost = interventions[i].Cost;
                model.Proposer = userService.GetUserByUserId(interventions[i].ProposerId).Name;
                model.PerformedDate = interventions[i].PerformedDate;
                model.State = interventions[i].State;
                if (interventions[i].ApproverId.HasValue)
                    model.Approver = userService.GetUserByUserId(interventions[i].ApproverId.GetValueOrDefault()).Name;
                model.Notes = interventions[i].Notes;
                model.RemainingLife = interventions[i].RemainingLife;
                model.RecentVist = interventions[i].RecentVist;
                clientDeatil.interventions.interventionModels.Add(model);
            }
            return clientDeatil;
        }

        public List<string> GetInterventionType()
        {
            List<string> type = new List<string>();
            var allType = typeService.GetAllInterventionTypes();
           foreach(var item in allType)
                type.Add(item.Name.ToString());
            return type;
        }

        public QualityManagementModel GetInterventionQualityManagementDetail(int id,int userId)
        {
            QualityManagementModel model = new QualityManagementModel();
            var user = userService.GetUserByUserId(userId);
            var intervention = interventionService.getInterventionById(id);
            var district = clientService.GetClientById(intervention.ClientId).District;
            if (user.District != district) return null;
            else
            {
                model.Notes = intervention.Notes;
                if (intervention.RemainingLife.HasValue)
                    model.RemainingLife = intervention.RemainingLife.GetValueOrDefault(100);
                if (intervention.RecentVist.HasValue)
                    model.RecentVist = intervention.RecentVist.GetValueOrDefault(DateTime.Today);
                return model;
            }
        }

        public List<string> getStateList(string state,int userId,int interventionId)
        {
            var intervention = interventionService.getInterventionById(interventionId);
            var user = userService.GetUserByUserId(userId);
            var type = typeService.GetInterventionTypeDetailByName(intervention.InterventionType);
            List<string> states = new List<string>();
            if(state=="Completed")
            {
                states = null;
            }
            else if(state=="Approved")
            {
                states.Add("Cancelled");
                states.Add("Completed");
            }
            else if(state=="Proposed")
            {
              if(user.MaximumCost>=intervention.Cost&&user.MaximumHours>=intervention.Labour&&user.MaximumCost>=type.EstimatedMaterials&&user.MaximumHours>=type.EstimatedLabour)
                {
                    states.Add("Cancelled");
                    states.Add("Approved");
                    states.Add("Completed");
                }
              else
                {
                    states.Add("Cancelled");
                }
            }
            return states;
        }

        public List<string> checkStateList(int userId,string interventionType,double cost,double labour)
        {
            List<string> states = new List<string>();
            var user = userService.GetUserByUserId(userId);
            var type = typeService.GetInterventionTypeDetailByName(interventionType);
            if (user.MaximumCost >= type.EstimatedMaterials && user.MaximumHours >= type.EstimatedLabour && user.MaximumHours >= labour && user.MaximumCost >= cost)
            {
                states.Add("Proposed");
                states.Add("Approved");
                states.Add("Completd");
            }
            else states.Add("Proposed");
            return states;
        }
    }
}