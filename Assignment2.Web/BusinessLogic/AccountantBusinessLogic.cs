﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment2.Web.Models;
using System.Web.Mvc;
using Assignment2.Service;
using Assignment2.Data;

namespace Assignment2.Web.Business_Logic
{
    public class AccountantBusinessLogic
    {
        public UserService userService;
        public DistrictService districtService;
        public ReportService reportservice;
        public AccountantBusinessLogic()
        {
            userService = new UserService();
            districtService = new DistrictService();
            reportservice = new ReportService();
        }

        public List<StaffModel> ListAllStaff()
        {
            var staff = userService.GetAllStaff().ToArray();
            List<StaffModel> models = new List<StaffModel>();
            for(int i=0;i<staff.Length;i++)
            {
                StaffModel model = new StaffModel();
                model.Name = staff[i].Name;
                model.Role = staff[i].UserType;
                model.District = staff[i].District;
                models.Add(model);
            }
            return models;
        }

        public List<string> getAllDistrict()
        {
            List<string> items= new List<string>();
            var districts = districtService.GetAllDistricts();
            foreach(var district in districts)
            {
                items.Add(district.Name);
            }
            return items;
        }

        public TotalDistrictCostModel getTotalDistrictCosts()
        {
            TotalDistrictCostModel model = new TotalDistrictCostModel();
            model.districtCosts = reportservice.getDistrictTotalCosts();
            foreach(var item in model.districtCosts)
            {
                model.CostGrandTotal += item.TotalCosts;
                model.LabourGrandTotal += item.TotalHours;
            }
            return model;
        }
    }
}