﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment2.Service;
using Assignment2.Web.Models;

namespace Assignment2.Web.Business_Logic
{
    public class AccountBusinessLogic
    {
        private UserService service;
        public AccountBusinessLogic()
        {
            service = new UserService();
        }
        public bool Login(LoginModel model)
        {
           var user=service.GetUserByUserName(model.UserName);
            if (user==null) return false;
            else
            {
                if (user.Password == model.Password)
                    return true;
                else return false;
            }
        }
    }
}