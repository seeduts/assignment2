﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment2.Service;
using Assignment2.Web.Models;
using System.Net.Mail;

namespace Assignment2.Web.BusinessLogic
{
    public class ManagerBusinessLogic
    {
        public InterventionSerivce interventionService;
        public UserService userService;
        public InterventionTypeService typeService;
        private ClientService clienService;
        public ManagerBusinessLogic()
        {
            interventionService = new InterventionSerivce();
            userService = new UserService();
            typeService = new InterventionTypeService();
            clienService = new ClientService();
        }

        public List<InterventionModel> checkRule(int managerId)
        {
            var interventions = interventionService.getInterventionsInManagerDistrict(managerId);
            var user = userService.GetUserByUserId(managerId);
            List <InterventionModel> model= new List<InterventionModel>();
            foreach(var item in interventions)
            {
                InterventionModel intervention = new InterventionModel();
                var type = typeService.GetInterventionTypeDetailByName(item.InterventionType);
                if (user.MaximumCost >= type.EstimatedMaterials && user.MaximumHours >= type.EstimatedLabour && user.MaximumHours > item.Labour && user.MaximumCost > item.Cost)
                {
                    intervention.Id = item.Id;
                    intervention.InterventionType = item.InterventionType;
                    intervention.Labour = item.Labour;
                    intervention.Cost = item.Cost;
                    intervention.Client = clienService.GetClientById(item.ClientId).Name;
                    intervention.Proposer = userService.GetUserByUserId(item.ProposerId).Name;
                    intervention.PerformedDate = item.PerformedDate;
                    intervention.State = item.State;
                    intervention.Notes = item.Notes;
                    intervention.RemainingLife = item.RemainingLife;
                    intervention.RecentVist = item.RecentVist;
                    model.Add(intervention);
                }
                else continue;
            }
            return model;
        }

        public List<string> getState()
        {
            List<string> states = new List<string>();
            states.Add("Cancelled");
            states.Add("Approved");
            return states;
        }

        public string SendEmail(int interventionId,ChangeStateModel model)
        {
            string message;
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("yaphets.xuchao@gmail.com");
                var intervention = interventionService.getInterventionById(interventionId);
                var user = userService.GetUserByUserId(intervention.ProposerId);
                var manager = userService.GetUserByUserId(intervention.ApproverId.Value).Name;
                mail.To.Add(user.EmailAddress);
                mail.Subject = "Notification.";
                mail.Body = "The intervention " + interventionId + " has just been "+model.state+" by "+manager+".";
                smtpServer.Port = 587;
                smtpServer.UseDefaultCredentials = false;
                smtpServer.Credentials = new System.Net.NetworkCredential("yaphets.xuchao@gmail.com", "cxc65337412");
                smtpServer.EnableSsl = true;
                smtpServer.Send(mail);
                message = "Send mail Successfully";
                return message;
            }
            catch(Exception ex)
            {
                message = ex.Message;
                return message;
            }
        }
    }
}