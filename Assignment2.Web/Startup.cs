﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Assignment2.Web.Startup))]
namespace Assignment2.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
