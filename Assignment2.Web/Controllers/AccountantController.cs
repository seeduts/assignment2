﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment2.Web.Models;
using Assignment2.Web.Business_Logic;
using Assignment2.Data;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace Assignment2.Web.Controllers
{
    [Authorize(Roles = "Accountant")]
    public class AccountantController : Controller
    {
        AccountantBusinessLogic accountantBusinessLogic;

        public AccountantController()
        {
            accountantBusinessLogic = new AccountantBusinessLogic();
        }
        // GET: Accountant
        public ActionResult Index()
        {
             return View("Index");
        }

        public ActionResult ViewStaff()
        {
            StaffModels staff = new StaffModels();
            staff.staffModels = accountantBusinessLogic.ListAllStaff();
            return PartialView("ViewStaff", staff);
        }

        [HttpGet]
        public ActionResult ChangeDistrict(string Name)
        {
            ChangeDistrictModel model = new ChangeDistrictModel();
            model.Name = Name;
            model.Districts = accountantBusinessLogic.getAllDistrict();
            return PartialView("ChangeDistrict", model);
        }

        [HttpPost]
        public ActionResult ChangeDistrict(ChangeDistrictModel model)
        {
            if (accountantBusinessLogic.userService.ChangeDistrict(model.Name, model.District))
            {
                TempData["notice"] = "Change district successfully.";
                return RedirectToAction("Index", "Accountant");
            }
            else return View();
        }

        [HttpGet]
        public ActionResult EngineerTotalCosts()
        {
            EngineerTotalCostsModel model = new EngineerTotalCostsModel(); 
            model.totalCostsModel=    accountantBusinessLogic.reportservice.getEngineerTotalCosts();
            return PartialView("EngineerTotalCosts", model);
        }

        [HttpGet]
        public ActionResult EngineerAverageCosts()
        {
            EngineerAverageCostsModel model = new EngineerAverageCostsModel();
            model.averageCostsModel = accountantBusinessLogic.reportservice.getEngineerAverageCosts();
            return PartialView("EngineerAverageCosts", model);
        }

        [HttpGet]
        public ActionResult DistrictTotalCosts()
        {
            TotalDistrictCostModel model = accountantBusinessLogic.getTotalDistrictCosts();
            return PartialView("DistrictTotalCosts", model);
        }

        [HttpGet]
        public ActionResult MonthlyDistrictCost()
        {
            MonthlyDistrictCostModel model = new MonthlyDistrictCostModel();
            model.Districts = accountantBusinessLogic.getAllDistrict();
            return PartialView("MonthlyDistrictCost", model);
        }

        [HttpPost]
        public ActionResult MonthlyDistrictCost(string name)
        {
            MonthlyDistrictCostModel model = new MonthlyDistrictCostModel();
            model.Districts = accountantBusinessLogic.getAllDistrict();
            model.District = name;
            model.model = accountantBusinessLogic.reportservice.getDistrictMonthlyCost(name);
            return PartialView("MonthlyDistrictCost", model);
        }
    }
}