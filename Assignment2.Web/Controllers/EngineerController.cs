﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment2.Web.Models;
using Assignment2.Service;
using Assignment2.Data;
using Assignment2.Web.Business_Logic;

namespace Assignment2.Web.Controllers
{
    [Authorize(Roles = "Site Engineer")]
    public class EngineerController : Controller
    {
        EngineerBusinessLogic engineerBusinessLogic;

        public EngineerController()
        {
            engineerBusinessLogic = new EngineerBusinessLogic();
        }
        // GET: Engineer
        public ActionResult Index()
        {
                return View("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateClient(ClientModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                    Client client = new Client();
                    client.Name = model.Name;
                    client.District = model.District;
                    client.LocationInfo = model.LocationInfo;
                    bool result=engineerBusinessLogic.clientService.Add(client);
                    if (result == true)
                    {
                        TempData["notice"] = "Create new client successfully";
                        return RedirectToAction("Index", "Engineer");
                    }
                    else return View();
            }
        }

        public ActionResult CreateClient()
        {
            return PartialView("CreateClient");
        }

        public ActionResult ViewClients()
        {          
            string district = Session["District"].ToString();
            ClientModels clients = new ClientModels();         clients.clientmodels=engineerBusinessLogic.GetClientsInEngineerDistrict(district);
                return PartialView("ViewClients",clients);
        }

        public ActionResult ViewInterventions()
        {
            int userId =(int)Session["UserId"];
            InterventionModels interventions = new InterventionModels();
            interventions.interventionModels = engineerBusinessLogic.GetInterventionsCreatedByUser(userId);
            return PartialView("ViewInterventions", interventions);
        }

        [HttpPost]
        public ActionResult ClientDetail(string name)
        {
            var clientInfo = engineerBusinessLogic.GetClientDetail(name);
            return PartialView("ClientDetail",clientInfo);
        }

        [HttpGet]
        public ActionResult CreateIntervention(string name)
        {
            int userId=(int)Session["UserId"];
            CreateInterventionModel model = new CreateInterventionModel();
            model.interventionTypes = engineerBusinessLogic.GetInterventionType();
            model.interventionType = model.interventionTypes.FirstOrDefault();
            model.Client = name;
          model.Labour = engineerBusinessLogic.typeService.GetInterventionTypeDetailByName(model.interventionType).EstimatedLabour;
           model.Cost = engineerBusinessLogic.typeService.GetInterventionTypeDetailByName(model.interventionType).EstimatedMaterials;
            model.states = engineerBusinessLogic.checkStateList(userId, model.interventionType, model.Cost, model.Labour);     
            return PartialView("CreateIntervention", model);
        }

        [HttpPost]
        public ActionResult CreateIntervention(CreateInterventionModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                Intervention intervention = new Intervention();
                intervention.ClientId = engineerBusinessLogic.clientService.GetClientByName(model.Client).Id;
                intervention.Cost = model.Cost;
                intervention.Labour = model.Labour;
                intervention.InterventionType = model.interventionType;
                intervention.ProposerId = (int)Session["UserId"];
                intervention.PerformedDate = model.PerformedDate;
                intervention.State = model.state;
                if (engineerBusinessLogic.interventionService.Add(intervention))
                {
                    TempData["notice"] = "Create new Intervention successfully.";
                    return RedirectToAction("Index", "Engineer");
                }
                else return View();
            }
        }

        [HttpGet]
        public ActionResult QualityManagement(int ID)
        {
            Session["intervention"] = ID;
            int userId = (int)Session["UserId"];
            QualityManagementModel model = engineerBusinessLogic.GetInterventionQualityManagementDetail(ID,userId);
            return PartialView("QualityManagement", model);
        }

        [HttpPost]
        public ActionResult QualityManagement(QualityManagementModel model)
        {
            string notes = model.Notes;
            int? life = model.RemainingLife;
            DateTime? time = model.RecentVist;
            int id = (int)Session["intervention"];
            if (engineerBusinessLogic.interventionService.edit(id, notes, life, time))
            {
                TempData["notice"] = "Change quality management information successfully.";
                return RedirectToAction("Index", "Engineer");
            }
            else return View();
        }

        [HttpGet]
        public ActionResult ChangeState(int ID,string State)
        {
            Session["intervention"] = ID;
            int userId =(int)Session["UserId"];
            ChangeStateModel model = new ChangeStateModel();
              model.states = engineerBusinessLogic.getStateList(State, userId, ID);
            return PartialView("ChangeState", model);
        }

        [HttpPost]
        public ActionResult ChangeState(ChangeStateModel model)
        {
            int id =(int)Session["intervention"];
            int userId = (int)Session["UserId"];
            string state = model.state;
            if (engineerBusinessLogic.interventionService.changeState(userId, state, id))
            {
                TempData["notice"] = "Change intervention state successfully.";
                return RedirectToAction("Index", "Engineer");
            }
            else return View();
        }

        [HttpGet]
        public ActionResult CheckStateByTypeChange(string Type,string Client)
        {
            int userId = (int)Session["UserId"];
            CreateInterventionModel model = new CreateInterventionModel();
            model.interventionTypes = engineerBusinessLogic.GetInterventionType();
            model.interventionType = Type;
            model.Cost = engineerBusinessLogic.typeService.GetInterventionTypeDetailByName(Type).EstimatedMaterials;
            model.Client = Client;
           model.Labour = engineerBusinessLogic.typeService.GetInterventionTypeDetailByName(Type).EstimatedLabour;
            model.states = engineerBusinessLogic.checkStateList(userId, model.interventionType, model.Cost, model.Labour);
            return PartialView("CreateIntervention", model);
        }
        
        [HttpGet]
        public ActionResult CheckStateByChangeCostOrLabour(string Type, string Client,double Labour,double Cost)
        {
            int userId = (int)Session["UserId"];
            CreateInterventionModel model = new CreateInterventionModel();
            model.interventionTypes = engineerBusinessLogic.GetInterventionType();
            model.interventionType = Type;
            model.Cost = Cost;
            model.Client = Client;
            model.Labour = Labour;
            model.states = engineerBusinessLogic.checkStateList(userId, model.interventionType, model.Cost, Labour);
            return PartialView("CreateIntervention", model);
        }
    }
}