﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment2.Web.Models;
using Assignment2.Web.Business_Logic;
using Assignment2.Service;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Assignment2.Data;

namespace Assignment2.Web.Controllers
{
    
    public class AccountController : Controller
    {
        private AccountBusinessLogic accountBusinessLogic;
        public AccountController()
        {
            accountBusinessLogic = new AccountBusinessLogic();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View("Login");
        }

        
        [HttpGet]
        [Authorize]
        public ActionResult ChangePassword()
        {
             return View("ChangePassword");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                if (accountBusinessLogic.Login(model) == false)
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    using (var service = new UserService())
                    {
                        var user = service.GetUserByUserName(model.UserName);
                        
                        Session["UserId"] = user.Id;
                        Session["Name"] = user.Name;
                        Session["District"] = user.District;
                        string role = Roles.GetRolesForUser(model.UserName)[0];
                        Session["Role"] = role;
                        if (role=="Site Engineer")
                        {
                            return RedirectToAction("Index", "Engineer");
                        }
                        else if(role == "Manager")
                        {
                            return RedirectToAction("Index", "Manager");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Accountant");
                        }
                    }
                }
            }
            return View();
        }

        [HttpPost]        
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            int userId = (int)Session["UserId"];
            string userType = Session["Role"].ToString();
            if (!ModelState.IsValid)
            {
                return View("ChangePassword");
            }
            else
            {
                using (var service = new UserService())
                {
                    service.ChangePassword(userId, model.NewPassword);
                    TempData["notice"] = "Change Password Successfully";
                }
                if (userType == "Site Engineer")
                {
                    return RedirectToAction("Index", "Engineer");
                }
                else if (userType == "Manager")
                {
                    return RedirectToAction("Index", "Manager");
                }
                else
                {
                    return RedirectToAction("Index", "Accountant");
                }
            }
        }
        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login","Account");
        }
    }
}
