﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment2.Web.Models;
using Assignment2.Web.BusinessLogic;

namespace Assignment2.Web.Controllers
{
    [Authorize(Roles = "Manager")]
    public class ManagerController : Controller
    {
        ManagerBusinessLogic managerBusinessLogic;

        public ManagerController()
        {
            managerBusinessLogic = new ManagerBusinessLogic();
        }
        // GET: Manager
        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpGet]
        public ActionResult ViewInterventions()
        {
            int userId =(int)Session["UserId"];
            InterventionModels model = new InterventionModels();
            model.interventionModels = managerBusinessLogic.checkRule(userId);
            return PartialView("ViewInterventions", model);
        }

        [HttpGet]
        public ActionResult ChangeInterventionState(int ID)
        {
            Session["intervention"] = ID;
            int userId = (int)Session["UserId"];
            ChangeStateModel model = new ChangeStateModel();
            model.states = managerBusinessLogic.getState();
            return PartialView("ChangeInterventionState", model);
        }

        [HttpPost]
        public ActionResult ChangeInterventionState(ChangeStateModel model)
        {
            int id = (int)Session["intervention"];
            int userId = (int)Session["UserId"];
            string state = model.state;
            if (managerBusinessLogic.interventionService.changeState(userId, state, id))
            {
                string message = managerBusinessLogic.SendEmail(id, model);
                TempData["notice"] = "Change intervention state successfully. " + message;            
                return RedirectToAction("Index", "Manager");
            }
            else return View();
        }

    }
}