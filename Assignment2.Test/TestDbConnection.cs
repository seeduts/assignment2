﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;

namespace Assignment2.Test
{
    [TestClass]
    public class TestDbConnection
    {
        [AssemblyInitialize]
        public static void SetupDataDirectory(TestContext context)
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.GetFullPath(@"..\..\..\Assignment2.Web\App_Data\"));
        }
        [TestMethod]
        public void TestOpenCloseSqlConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["InterventionContext"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            conn.Open();
            conn.Close();
        }
    }
}
