﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment2.Service;
using Assignment2.Data;
using System.Transactions;

namespace Assignment2.Test.TestService
{
    [TestClass]
    public  class TestClientService
    {
        [TestMethod]
        public void TestGetClientsInSameDistrict()
        {
            using (var service = new ClientService())
            {
                var Clients = service.GetClientsInSameDistrict("Rural Indonesia");
                Assert.AreEqual(1, Clients.Count);
            }
        }

        [TestMethod]
        public void TestGetClientById_IsExist()
        {
            using (var service = new ClientService())
            {
                var Client = service.GetClientById(5);
                Assert.IsNotNull(Client);
            }
        }

        [TestMethod]
        public void TestGetClientById_IsNotExist()
        {
            using (var service = new ClientService())
            {
                var Client = service.GetClientById(1);
                Assert.IsNull(Client);
            }
        }

       [TestMethod]
        public void TestAddClient()
        {
            Client client = new Client();
            client.District ="Rural Indonesia";
            client.Name = "Bon";
            client.LocationInfo = "abcd street";
            using (var service = new ClientService())
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    bool result = service.Add(client);
                    Assert.AreEqual(true, result);
                }
            }
        }

        [TestMethod]
        public void TestGetClientByNameIsExit()
        {
            using (var service = new ClientService())
            {
                var Client = service.GetClientByName("abc");
                Assert.IsNotNull(Client);
            }
        }
    }
}
