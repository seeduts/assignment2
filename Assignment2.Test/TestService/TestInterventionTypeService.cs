﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment2.Service;

namespace Assignment2.Test.TestService
{
    [TestClass]
    public class TestInterventionTypeService
    {
        [TestMethod]
        public void TestGetAllInterventionTypes()
        {
            using (var service = new InterventionTypeService())
            {
                var InterventionTypes = service.GetAllInterventionTypes();
                Assert.AreEqual(7, InterventionTypes.Count);
            }
        }

        [TestMethod]
        public void TestGetInterventionTypeDetailByName_IsExist()
        {
            using (var service = new InterventionTypeService())
            {
                var InterventionType = service.GetInterventionTypeDetailByName("Supply and Install Portable Toilet");
                Assert.IsNotNull(InterventionType);
            }
        }

        [TestMethod]
        public void TestGetInterventionTypeDetailByName_IsNotExist()
        {
            using (var service = new InterventionTypeService())
            {
                var InterventionType = service.GetInterventionTypeDetailByName("asdfasdf");
                Assert.IsNull(InterventionType);
            }
        }
    }
}
