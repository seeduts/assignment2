﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Service;
using System.Transactions;

namespace Assignment2.Test.TestService
{
    [TestClass]
    public class TestUserService
    {
        [TestMethod]
        public void TestGetAllStaff()
        {
            using (var service = new UserService())
            {
                var staff = service.GetAllStaff();
                Assert.AreEqual(5,staff.Count);
            }
        }

        [TestMethod]
        public void TestGetUserByUserId_IsExist()
        {
            using (var service = new UserService())
            {
                var user = service.GetUserByUserId(13);
                Assert.IsNotNull(user);
            }
        }

        [TestMethod]
        public void TestGetUserByUserId_IsNotExist()
        {
            using (var service = new UserService())
            {
                var user = service.GetUserByUserId(123);
                Assert.IsNull(user);
            }
        }

        [TestMethod]
        public void TestGetUserByUserName_IsExist()
        {
            using (var service = new UserService())
            {
                var user = service.GetUserByUserName("alice@example.com");
                Assert.IsNotNull(user);
            }
        }

        [TestMethod]
        public void TestGetUserByUserName_IsNotExist()
        {
            using (var service = new UserService())
            {
                var user = service.GetUserByUserName("sfsfsf");
                Assert.IsNull(user);
            }
        }

       [TestMethod]
        public void TestChangePassword()
        {
            using (var service = new UserService())
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    string password = "123";
                    bool result = service.ChangePassword(12,password);
                    Assert.AreEqual(true, result);
                }
            }
        }

        [TestMethod]
        public void TestChangeDistrictChange()
        {
            using (var service = new UserService())
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    var user = service.GetUserByUserId(12);
                   string district = "Sydney";
                    bool result = service.ChangeDistrict(user.UserName,district);
                    Assert.AreEqual(false, result);
                }
            }
        }
    }
}
