﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;
using Assignment2.Service;
using System.Transactions;

namespace Assignment2.Test.TestService
{
    [TestClass]
    public class TestInterventionService
    {
        [TestMethod]
        public void TestAddIntervention()
        {
            Intervention intervention = new Intervention();
            intervention.InterventionType = "Hepatitis Avoidance Training";
            intervention.ClientId = 5;
            intervention.Labour = 3;
            intervention.Cost = 0;
            intervention.ProposerId = 13;
            intervention.PerformedDate = new DateTime(2016, 05, 18);
            intervention.State = "Proposed";
            using (TransactionScope scope = new TransactionScope())
            {
                using (var service = new InterventionSerivce())
                {
                    bool result = service.Add(intervention);
                    Assert.AreEqual(true, result);
                }
            }
        }

        [TestMethod]
        public void TestgetInterventionsByClientId()
        {
            using (var service = new InterventionSerivce())
            {
                var intervention = service.getInterventionsByClientId(5);
                Assert.AreEqual(1, intervention.Count);
            }
        }

        [TestMethod]
        public void TestgetInterventionById()
        {
            using (var service = new InterventionSerivce())
            {
                var intervention = service.getInterventionById(9);
                Assert.IsNotNull(intervention);
            }
        }

        [TestMethod]
        public void TestgetInterventionByUserId()
        {
            using (var service = new InterventionSerivce())
            {
                var intervention = service.getInterventionByUserId(12);
                Assert.IsNotNull(intervention);
            }
        }
    }
}
