﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Assignment2.Test.TestService
{
    [TestClass]
    public class TestDistrictService
    {
        [TestMethod]
        public void TestGetAllDistricts()
        {
            using (var service = new DistrictService())
            {
                var Districts = service.GetAllDistricts();
                Assert.AreEqual(6, Districts.Count);
            }
        }
    }
}
