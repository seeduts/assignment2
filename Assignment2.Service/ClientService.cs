﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;

namespace Assignment2.Service
{
    public class ClientService:BaseService
    {
       public ClientService():base()
        {
        }

        public ICollection<Client> GetClientsInSameDistrict(string District)
        {
            return Context.Clients.Where(x => x.District == District).ToList();
        }

        public Client GetClientById(int ClientId)
        {
            return Context.Clients.Find(ClientId);
        }

        public Client GetClientByName(string name)
        {
            return Context.Clients.Where(x => x.Name == name).FirstOrDefault();
        }
        public bool Add(Client client)
        {
            if (client != null)
            {
                Context.Clients.Add(client);
                Context.SaveChanges();
                return true;
            }
            else return false;
        }
    }
}
