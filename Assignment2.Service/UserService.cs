﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;

namespace Assignment2.Service
{
    public class UserService:BaseService
    {
        public UserService():base()
       {
       }

        public  ICollection<User> GetAllStaff()
        {
            var staff = Context.Users.Where(x => x.UserType != "Accountant");
            return staff.ToList();
        }

        public  User GetUserByUserId(int userId)
        {
            return Context.Users.Find(userId);
        }

        public User GetUserByUserName(string userName)
        {

            var user = Context.Users.Where(x => x.UserName == userName).FirstOrDefault();
            return user;
        }

        public User GetUserByName(string Name)
        {
            return Context.Users.Where(x => x.Name == Name).FirstOrDefault();
        }

        public bool ChangeDistrict(string name,string district)
        {
            User user = GetUserByName(name);
            if (user != null)
            {
                user.District = district;
                Context.SaveChanges();
                return true;
            }
            else return false;
        }

        public bool ChangePassword(int userId,string password)
        {
            User user = GetUserByUserId(userId);
            if (user != null)
            {
                  user.Password = password;
                  Context.SaveChanges();
                  return true;
             }
              else return false;
        }
    }
}
