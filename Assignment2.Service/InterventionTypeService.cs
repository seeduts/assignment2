﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;

namespace Assignment2.Service
{
    public class InterventionTypeService:BaseService
    {
        public InterventionTypeService():base()
        { }

        public ICollection<InterventionType> GetAllInterventionTypes()
        {
            return Context.InterventionTypes.ToList();
        }

        public InterventionType GetInterventionTypeDetailByName(string interventionType)
        {
            return Context.InterventionTypes.Find(interventionType);
        }
    }
}
