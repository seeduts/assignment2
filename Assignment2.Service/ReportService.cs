﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;


namespace Assignment2.Service
{
    public class ReportService:InterventionSerivce
    {
        public ReportService():base()
        { }

        public List<EngineerTotalCosts> getEngineerTotalCosts()
        {
            var result = Context.Interventions.Join(Context.Users, i => i.ProposerId, u => u.Id, (i, u) => new { i, u }).Where(i => i.i.State == "Completed"&&i.u.UserType=="Site Engineer").GroupBy(x => new { x.i.ProposerId, x.u.Name}).Select(re => new { Name = re.Key.Name, laboursum = re.Sum(i => i.i.Labour), costsum = re.Sum(i => i.i.Cost) }).OrderBy(u=>u.Name).ToList();
            List<EngineerTotalCosts> model = new List<EngineerTotalCosts>();
            foreach (var item in result)
            {
                EngineerTotalCosts total = new EngineerTotalCosts();
                total.Engineer = item.Name;
                total.TotalHours = item.laboursum;
                total.TotalCosts = item.costsum;
                model.Add(total);
            }
            return model;    
        }
        public List<EngineerAverageCosts> getEngineerAverageCosts()
        {
            var result= Context.Interventions.Join(Context.Users, i => i.ProposerId, u => u.Id, (i, u) => new { i, u }).Where(i => i.i.State == "Completed" && i.u.UserType == "Site Engineer").GroupBy(x => new { x.i.ProposerId, x.u.Name }).Select(re => new { Name = re.Key.Name, labouravg = re.Average(i => i.i.Labour), costavg = re.Average(i => i.i.Cost) }).OrderBy(u => u.Name).ToList();
            List<EngineerAverageCosts> model = new List<EngineerAverageCosts>();
            foreach (var item in result)
            {
                EngineerAverageCosts avg = new EngineerAverageCosts();
                avg.Engineer = item.Name;
                avg.AverageHours = item.labouravg;
                avg.AverageCosts = item.costavg;
                model.Add(avg);
            }
            return model;
        }

        public List<TotalDistrictCosts> getDistrictTotalCosts()
        {
            var res = Context.Interventions.Where(i => i.State == "Completed").Join(Context.Clients, i => i.ClientId, c => c.Id, (i, c) => new { i, c }).Join(Context.Districts, cc => cc.c.District, d => d.Name, (cc, d) => new { cc, d }).GroupBy(d => d.d.Name).Select(re => new { Name = re.Key, laboursum = re.Sum(c => c.cc.i.Labour), costsum = re.Sum(c => c.cc.i.Cost) }).ToList();
            List<TotalDistrictCosts> totalCosts = new List<TotalDistrictCosts>();
            foreach(var item in res)
            {
                TotalDistrictCosts total = new TotalDistrictCosts();
                total.District = item.Name;
                total.TotalCosts = item.costsum;
                total.TotalHours = item.laboursum;
                totalCosts.Add(total);
            }
            return totalCosts;
        }

      public List<MonthlyDistrictCost> getDistrictMonthlyCost(string district)
     {
            var res = Context.Interventions.Where(i => i.State == "Completed").Join(Context.Clients, i => i.ClientId, c => c.Id, (i, c) => new { i, c }).Where(c => c.c.District == district).GroupBy(x =>x.i.PerformedDate).Select(re => new { laboursum = re.Sum(i => i.i.Labour), costsum = re.Sum(i => i.i.Cost),date=re.Key}).ToList();
            List<MonthlyDistrictCost> model = new List<MonthlyDistrictCost>();
            foreach (var item in res)
            {
                MonthlyDistrictCost monthly = new MonthlyDistrictCost();
                monthly.monthlyCostTotal = item.costsum;
                monthly.monthlyLabourTotal = item.laboursum;
                monthly.month = item.date.Month.ToString() + "/" + item.date.Year.ToString();
                model.Add(monthly);
            }
            return model;
     }
    }
}
