﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;

namespace Assignment2.Service
{
    public class InterventionSerivce:BaseService
    {
        public InterventionSerivce():base()
        { }
        public ICollection<Intervention> getInterventionsByClientId(int ClientId)
        {
            var intervention = Context.Interventions.Where(i => i.ClientId == ClientId&&i.State!="Cancelled");
            return intervention.ToList();
        }

        public bool Add(Intervention intervention)
        {
            Context.Interventions.Add(intervention);
            if (Context.SaveChanges() > 0) return true;
            else return false;
        }

        public Intervention getInterventionById(int interventionId)
        {
            return Context.Interventions.Find(interventionId);
        }

        public ICollection<Intervention> getInterventionByUserId(int userId)
        {
            return Context.Interventions.Where(x => x.ProposerId == userId&&x.State!="Cancelled").ToList();
        }

        public bool edit(int id,string notes,int? life,DateTime? date)
        {
            Intervention intervention = getInterventionById(id);
            if (intervention != null)
            {
                intervention.Notes = notes;
                intervention.RemainingLife = life;
                intervention.RecentVist = date;
                Context.SaveChanges();
                return true;
            }
            else return false;
        }

        public bool changeState(int userId,string state,int id)
        {
            Intervention intervention = getInterventionById(id);
            if (intervention != null)
            {
                intervention.ApproverId = userId;
                intervention.State = state;
                Context.SaveChanges();
                return true;
            }
            else return false;
        }

        public List<Intervention> getInterventionsInManagerDistrict(int ManagerId)
        {
            var result = Context.Interventions.Where(i => i.State == "Proposed").Join(Context.Clients, i => i.ClientId, c => c.Id, (i, c) => new { i, c }).Join(Context.Users, cc => cc.c.District, u => u.District, (cc, u) => new { cc, u }).Where(u => u.u.Id == ManagerId).GroupBy(x => new { x.cc.i.Id, x.u.District }).Select(re => re.Key.Id).ToList();
            List<Intervention> interventions = new List<Intervention>();
            foreach(var item in result)
            {
                Intervention intervention = getInterventionById(item);
                interventions.Add(intervention);
            }
            return interventions;
        }
    }
}
