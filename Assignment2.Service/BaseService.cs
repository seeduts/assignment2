﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;

namespace Assignment2.Service
{
    public class BaseService:IDisposable
    {
        bool selfCreated;
        protected InterventionContext Context;
        public BaseService()
        {
            selfCreated = true;
            Context = new InterventionContext();
        }

        public BaseService(InterventionContext Context)
        {
            if(Context!=null)
            {
                selfCreated = false;
                this.Context = new InterventionContext();
            }
            else
            {
                selfCreated = true;
                this.Context = new InterventionContext();
            }
        }
        public void Dispose()
        {
            if (selfCreated)
                Context.Dispose();
        }
    }
}
