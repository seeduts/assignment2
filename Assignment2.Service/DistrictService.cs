﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment2.Data;

namespace Assignment2.Service
{
    public class DistrictService:BaseService
    {
        public DistrictService():base()
            {}
        public ICollection<District> GetAllDistricts()
        {
            return Context.Districts.ToList();
        }
    }
}
